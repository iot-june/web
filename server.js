'use strict';

const mongo = require('mongodb');
const http = require('http');
var express = require('express');
var jwt = require('jsonwebtoken');
var lodash = require('lodash');
const passportJWT = require("passport-jwt");
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var i18n = require('i18n');
var LocalStrategy = require('passport-local').Strategy;
const nconf = require('nconf');
const mongoose = require('mongoose');
const mongoDatabase = nconf.get('mongoDatabase');
const MongoStore = require('connect-mongo')(session);
let uri = `mongodb://admin:12345@rentcross-shard-00-00-216zg.gcp.mongodb.net:27017,rentcross-shard-00-01-216zg.gcp.mongodb.net:27017,rentcross-shard-00-02-216zg.gcp.mongodb.net:27017/test?ssl=true&replicaSet=rentcross-shard-0&authSource=admin&retryWrites=true`;
if (mongoDatabase) {
    uri = `${uri}/${mongoDatabase}`;
}
console.log(uri);

mongo.MongoClient.connect(uri, (err, db) => {
    if (err) {
        throw err;
    }

    mongoose.connect(uri, { useNewUrlParser: true });

    var db = mongoose.connection;

    var routes = require('./routes/index');
    var users = require('./routes/users');
    var apartments = require('./routes/apartments')
    var app = express();

    app.set('views', path.join(__dirname, 'views'));
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    
    //app.use(express.static('public'));  
    app.use('/static', express.static('public'));


    i18n.configure({
      locales:['en', 'ua'],
      directory: __dirname + '/locales',
      defaultLocale: 'en',
      cookie: 'lang',
    });

    app.use(i18n.init);
    app.engine('handlebars',
                exphbs({ defaultLayout: 'layout',
                         helpers: {
                         i18n: function(){
                            return i18n.__.apply(this, arguments);
                         },
                     }
                }));
    app.set('view engine', 'handlebars');
    app.use(express.static(path.join(__dirname, 'public')));


    app.use(session({
        secret: 'secret',
        saveUninitialized: true,
        resave: true
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    app.use(expressValidator({

        errorFormatter: function (param, msg, value) {

            var namespace = param.split('.'),
                root = namespace.shift(),
                formParam = root;

            while (namespace.length) {

                formParam += '[' + namespace.shift() + ']';

            }
            return {
                param: formParam,
                msg: msg,
                value: value
            };

        }

    }));

    app.use(flash());

    app.use(function (req, res, next) {
        res.locals.success_msg = req.flash('success_msg');
        res.locals.error_msg = req.flash('error_msg');
        res.locals.error = req.flash('error');
        res.locals.user = req.user || null;
        next();
    });

    app.use('/', routes);
    app.use('/users', users);
    app.use('/apartments', apartments)

    var User = require('./models/user')
    var Apartment = require('./models/apartment')

    app.get('/api/users', function (req, res) {
        User.getUsers(function (err, users) {
            if (err) {
                throw err;
            }
            res.json(users);
        });

    });

    app.put('/api/users/:_id', (req, res) => {
        var id = req.params._id;
        var user = req.body;
        User.updateUser(id, user, {}, (err, user) => {
            if (err) {
                throw err;
            }
            res.json(user);
        });
    });

    app.delete('/api/users/:_id', (req, res) => {
        var id = req.params._id;
        User.deleteUser(id, (err, user) => {
            if (err) {
                throw err;
            }
            res.json(user);
        });
    });

    app.get('/api/users/:_id', (req, res) => {
        User.getUserById(req.params._id, (err, user) => {
            if (err) {
                throw err;
            }
            res.json(user);
        });
    });

    let jwtOptions = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: "secret"
    };
    let strategy = new JwtStrategy(jwtOptions, (jwt_payload, next) => {
        User.getUserById(req.params.id, (err, user) => {
            if (err) {
                throw err;
            }
            res.json(user);
            if (user) {
                next(null, user);
                console.log("payload received", jwt_payload);
            } else {
                next(null, false);
            }
        });
    });

    passport.use(strategy);

    app.post("/login", (req, res) => {
        if (req.body.username && req.body.password) {
            var username = req.body.username;
            var password = req.body.password;
        }
        User.getUserByUsername(username, function (err, user) {

            if (err) throw err;
            if (!user) {
                res.status(401).json({ message: "no such user found" });
            }
            User.comparePassword(req.body.password, user.password, function (err, isMatch) {

                if (err) throw err;
                if (isMatch) {
                    let payload = { id: user._id };
                    let token = jwt.sign(payload, jwtOptions.secretOrKey);
                    res.json({ message: "OK", token });
                } else {

                    res.status(401).json({ message: "password did not match" });
                }
            });

        });
    });


    app.get('/api/apartments', function (req, res) {
        Apartment.getApartments(function (err, apartments) {
            if (err) {
                throw err;
            }
            res.json(apartments);
        });

    });

    app.put('/api/apartments/:_id', (req, res) => {
        var id = req.params._id;
        var apartment = req.body;
        Apartment.updateApartment(id, apartment, {}, (err, apartment) => {
            if (err) {
                throw err;
            }
            res.json(apartment);
        });
    });

    app.delete('/api/apartments/:_id', (req, res) => {
        var id = req.params._id;
        Apartment.deleteApartment(id, (err, apartment) => {
            if (err) {
                throw err;
            }
            res.json(apartment);
        });
    });

    app.get('/api/apartments/:_id', (req, res) => {
        Apartment.getApartmentById(req.params._id, (err, apartment) => {
            if (err) {
                throw err;
            }
            res.json(apartment);
        });

    });

    app.get('/api/coordinates', function (req, res) {
        Apartment.getCoordinates(function (err, coordinates) {
            if (err) {
                throw err;
            }
            var features = [];
            for (var i = 0; i < coordinates.length; i++) {
                features.push({
                    type: 'Feature',
                    geometry: {
                        type: 'Point',
                        coordinates: coordinates[i].coordinates
                    },
                    properties: {
                        title: coordinates[i].title,
                        address: coordinates[i].address,
                        price: coordinates[i].price,
                        amenities: coordinates[i].amenities,
                    }
                });
            }
            var geoJsonData = {
                type: 'FeatureCollection',
                features: features
            }
            res.json(geoJsonData);
        });
    });

    app.set('port', (process.env.PORT || 3000));
    app.listen(app.get('port'), function () {

        console.log('Server started on port', +app.get('port'));

    });
});