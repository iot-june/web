var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var ApartmentSchema = mongoose.Schema({

	title: {
		type: String,
		index: true
	},
	type: String,
	address: String,
	coordinates: Array,
	floor: Number,
	number_of_rooms: Number,
	photos: Array,
	price: Number,
	award: Number,
	description: String,
	amenities: Array,
	renters_policy: Array,
	available_from: {
		type: Date,
		default: Date.now
	},
	available_visit_hours: Array,
	auto_accept: Boolean,
	author: ObjectId
});

var Apartment = module.exports = mongoose.model('Apartment', ApartmentSchema);

// Get Apartments
module.exports.getApartments = function (callback, limit) {
	Apartment.find(callback).limit(limit);
}

module.exports.getCoordinates = function(callback) {
    Apartment.find({}, 'title address price amenities coordinates', callback)
}

// Add Apartment
module.exports.addApartment = function (apartment, callback) {
	Apartment.create(apartment, callback);
}

// Update Apartment
module.exports.updateApartment = function (id, apartment, options, callback) {
	var query = { _id: id };
	var update = {
		title: apartment.title,
		type: apartment.type,
		address: apartment.address,
		coordinates: apartment.coordinates,
		floor: apartment.floor,
		number_of_rooms: apartment.number_of_rooms,
		photos: apartment.photos,
		price: apartment.price,
		award: apartment.award,
		description: apartment.description,
		amenities: apartment.amenities,
		renters_policy: apartment.renters_policy,
		available_from: apartment.available_from,
		available_visit_hours: apartment.available_visit_hours,
		auto_accept: apartment.auto_accept,
		author: apartment.author
	}
	Apartment.findOneAndUpdate(query, update, options, callback);
}

// Delete Apartment
module.exports.deleteApartment = function (id, callback) {
	var query = { _id: id };
	Apartment.remove(query, callback);
}