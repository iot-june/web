var express = require('express');
var router = express.Router();


var Apartment = require('../models/apartment');

router.get('/post_apartment', function(req,res){
    console.log("Here supposed to be a form");
});

router.post ('/post_apartment', function(req,res){

		var title = req.body.title;
		var type = req.body.type;
		var address = req.body.address;
		var coordinates = req.body.coordinates;
		var floor = req.body.floor;
        var number_of_rooms = req.body.number_of_rooms;
		var photos = req.body.photos;
		var price = req.body.price;
		var award = req.body.award;
		var description = req.body.description;
		var amenities = req.body.amenities;
        var renters_policy = req.body.renters_policy;
		var available_from = req.body.available_from;
		var available_visit_hours = req.body.available_visit_hours;
		var auto_accept = req.body.auto_accept;
		var author = req.body.author;

	var errors = req.validationErrors();

	if(errors){
	    console.log(errors)
	}
	else{

		var newApartment = new Apartment({
            title : title,
            type : type,
            address : address,
            coordinates: coordinates,
            floor : floor,
            number_of_rooms: number_of_rooms,
            photos : photos,
            price : price,
            award : award,
            description: description,
            amenities: amenities,
            renters_policy: renters_policy,
            available_from: available_from,
            available_visit_hours : available_visit_hours,
            auto_accept: auto_accept,
            author: author
		});

		Apartment.addApartment(newApartment , function(err , apartment){
			if(err) throw err ;
			console.log(apartment);
		});

		req.flash('success_msg',"Success");

		res.redirect('/');
	}
});


module.exports = router;